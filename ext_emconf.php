<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "gb_events".
 *
 * Auto generated 13-07-2017 12:45
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Event calendar',
	'description' => 'A simple calendar for upcoming events.',
	'category' => 'plugin',
	'author' => 'Morton Jonuschat, Markus Timtner',
	'author_email' => 'm.jonuschat@gute-botschafter.de, markus@timtner.de',
	'author_company' => 'Gute Botschafter GmbH',
	'state' => 'stable',
	'uploadfolder' => true,
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '8.7.1',
	'constraints' => 
	array (
		'depends' => 
		array (
			'php' => '5.5.0-7.1.99',
			'typo3' => '6.2.0-8.7.99',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
	'autoload' => 
	array (
		'psr-4' => 
		array (
			'GuteBotschafter\\GbEvents\\' => 'Classes',
		),
	),
	'autoload-dev' => 
	array (
		'psr-4' => 
		array (
			'GuteBotschafter\\GbEvents\\Tests\\' => 'Tests',
			'TYPO3\\CMS\\Core\\Tests\\' => '.Build/vendor/typo3/cms/typo3/sysext/core/Tests/',
			'TYPO3\\CMS\\Fluid\\Tests\\' => '.Build/vendor/typo3/cms/typo3/sysext/fluid/Tests/',
		),
	),
	'comment' => 'Make it possible to show events that have started but not yet finished.',
	'user' => 'gutebotschafter',
	'clearcacheonload' => false,
);