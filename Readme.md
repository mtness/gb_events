>"Come what come may, Time and the hour runs through the roughest day."
>(Macbeth)

Hi there. This is the mtness fork of gb_events.

Since development of the orginal ext has somewhat stalled, I decided to fork this nifty little calendar extension and to give it some love.

The most important fix is the Archive view - Then I added a typoscript template selector tool as known from ext:news, and last but not least I fixed the .ics date (as some other forks have, too).
~~Eventually this fork will be compatible with TYPO3 v8, I will notify about this here.~~
This fork is comapible with  TYPO32 v8 now, and seems to work with v9, too. 

mtness, ~~July 2017~~ January 2019





8.7.1 / 2018-12-06+
==================

  * [FEATURE] v8 compatibility done!
  * [BUGFIX] optimized caching 
  * [FEATURE] makecategorizable
  * [TASK] misc cleanup & touchup


7.6.1 / 2017-07-13
==================

  * [BUGFIX] .ics export file time fix
  * [BUGFIX] [FEATURE] Archive mode working now
  * [FEATURE] FE plugin typoscript template selector added
  * [FEATURE] GroupedForDateTimeViewHelper added

[OLD]

7.0.1 / 2016-01-20
==================

  * [BUGFIX] Re-enable RTE for description field

7.0.0 / 2016-01-07
==================

  * [FEATURE] Add upgrade wizard to migrate config from v6.x to v7.0
  * [BUGFIX] Correct SQL statement for categories in upcoming plugin
  * [BUGFIX] Add missing translations
  * [TASK] Switch to template/layout/partialRootPaths
  * [BUGFIX] FAL wizard fixes event attachments if all files for a record are missing
  * [TASK] Bump PHP requirements to PHP 5.5+
  * [TASK] Migrate TCA configuration
  * [TASK] Add composer & autoloading support
  * [TASK] Update templates to use introduction package styles
  * [TASK] Document all settings, cleanup documentation formatting
  * [FEATURE] Make categories available on Event model
  * [TASK] XLIFF 1.2 XML namespace
  * [TASK] Reformat to PSR-2 code style
  * [TASK] Update copyright notices
  * [BUGFIX] Fix annotations
  * [BUGFIX] Reformat SQL file to install tool expectations
  * [BUGFIX] Fix translations in Flexforms
  * [BUGFIX] Show all event dates in calendar view
  * [BUGFIX] Properly memoize the DataMapper
  * [BUGFIX] Fix for reversed DI order on TYPO3 6.2
  * [FEATURE] Add cache tags to pages for automatic cache clear
  * [TASK] Integrate FAL Migration Wizard into Install Tool
  * [FEATURE] Use FAL for file attachments
  * [FEATURE] Filter events by category
  * [FEATURE] Make events categorizable
  * [FEATURE] Support dedicated page for event details view
  * [TASK] Eliminate code smells and warnings
  * [TASK] Reformat according to CGL requirements
  * [FEATURE] Use File Abstraction Layer (FAL) for images and downloads
  * [FEATURE] Support categories for events
  * [FEATURE] Optionally show started events until the end date
  * [FEATURE] Archive view for events
  * [FEATURE] Split output modes into dedicated plugins
  * [FEATURE] Support dedicated single view page
  * [FEATURE] Support TYPO3 7.x
  * [BUGFIX] Conform to RFC2445 for iCal export
  * [BUGFIX] Make iCal export Outlook 2007 compatible