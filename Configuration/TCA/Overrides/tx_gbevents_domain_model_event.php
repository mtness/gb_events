<?
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
    'gb_events',
    'tx_gbevents_domain_model_event',
    'categories',
    []
);
